function _Location(tekst, zdjecie, kolor, warunek, item, owca) {
    this.tekst = tekst;
    this.zdjecie = zdjecie;
    this.kolor = kolor;
    this.warunek = warunek;
    this.item = item;
    this.owca = owca
}
document.addEventListener("DOMContentLoaded", function (event) {
    document.getElementById("cmdinput").focus() //focus na input po wejsciu na stronę
    game = {
        ok: 7, //do liczenia czesci owcy
        eq: ["a PRIZE", "1", "PRIZE", 36],
        items: {
            10: ["a KEY", "1", "KEY", 10],
            11: ["an AXE", "1", "AXE", 11],
            12: ["STICKS", "1", "STICKS", 12],
            13: ["sheeplegs", "0", "sheeplegs", 13],
            14: ["MUSHROOMS", "1", "MUSHROOMS", 14],
            15: ["MONEY", "1", "MONEY", 15],
            16: ["a BARREL", "1", "BARREL", 16],
            17: ["a sheeptrunk", "0", "sheeptrunk", 17],
            18: ["BERRIES", "1", "BERRIES", 18],
            19: ["WOOL", "1", "WOOL", 19],
            20: ["a sheepskin", "0", "sheepskin", 20],
            21: ["a BAG", "1", "BAG", 21],
            22: ["a RAG", "1", "RAG", 22],
            23: ["a sheephead", "0", "sheephead", 23],
            24: ["a SPADE", "1", "SPADE", 24],
            25: ["SULPHUR", "1", "SULPHUR", 25],
            26: ["a solid poison", "0", "solid poison", 26],
            27: ["a BUCKET", "1", "BUCKET", 27],
            28: ["TAR", "1", "TAR", 28],
            29: ["a liquid poison", "0", "liquid poison", 29],
            30: ["a dead dragon", "0", "dead dragon", 30],
            31: ["a STONE", "1", "STONE", 31],
            32: ["a FISH", "1", "FISH", 32],
            33: ["a KNIFE", "1", "KNIFE", 33],
            34: ["a DRAGONSKIN", "1", "DRAGONSKIN", 34],
            35: ["a dragonskin SHOES", "1", "SHOES", 35],
            36: ["a PRIZE", "1", "PRIZE", 36],
            37: ["a SHEEP", "1", "SHEEP", 37]
        },
        board: [
            [new _Location("You are inside a brimstone mine", "11.gif", "rgb(235,211,64)", ";;;E", [], []),//1
            new _Location("You are at the entrance to the mine", "12.gif", "rgb(89,93,87)", ";;W;E", [], []),
            new _Location("A hill", "13.gif", "rgb(117,237,243)", ";S;W;E", [["a STONE", "1", "STONE", 31]], []),
            new _Location("Some bushes", "14.gif", "rgb(202,230,51)", ";;W;E", [], []),
            new _Location("An old deserted hut", "15.gif", "rgb(220,204,61)", ";;W;E", [["a BUCKET", "1", "BUCKET", 27]], []),
            new _Location("The edge of a forest", "16.gif", "rgb(167,245,63)", ";;W;E", [], []),
            new _Location("A dark forest", "17.gif", "rgb(140,253,99)", ";S;W;", [["MUSHROOMS", "1", "MUSHROOMS", 14]], [])],
            [new _Location("A man nearby making tar", "21.gif", " rgb(255,190,99)", ";S;E;", [], []),//2
            new _Location("A timber yard", "22.gif", "rgb(255,190,99)", "S;;W;E", [], []),
            new _Location("You are by a roadside shrine", "23.gif", "rgb(167,245,63)", "N;S;W;E", [["a KEY", "1", "KEY", 10]], []),
            new _Location("You are by a small chapel", "24.gif", "rgb(212,229,36)", ";;W;E", [], []),
            new _Location("You are on a road leading to a wood", "25.gif", "rgb(167,245,63)", ";S;W;E", [], []),
            new _Location("You are in a forest", "26.gif", "rgb(167,245,63)", ";;W;E", [], []),
            new _Location("You are in a deep forest", "27.gif", "rgb(140,253,99)", "N;;W;", [["BERRIES", "1", "BERRIES", 18]], [])],
            [new _Location("You are by the Vistula River", "31.gif", "rgb(122,232,252)", "N;;;E", [], []),//3
            new _Location("You are by the Vistula River", "32.gif", "rgb(140,214,255)", "N;;W;", [["a FISH", "1", "FISH", 32]], []),
            new _Location("You are on a bridge over river", "33.gif", "rgb(108,181,242)", "N;S;;", [], []),
            new _Location("You are by the old tavern", "34.gif", "rgb(255,189,117)", ";;;E", [], []),
            new _Location("You are at the town's end", "35.gif", "rgb(255,190,99)", "N;S;W;", [], []),
            new _Location("You are in a butcher's shop", "36.gif", "rgb(255,188,102)", ";S;;", [], []),
            new _Location("You are in a cooper's house", "37.gif", "rgb(255,188,102)", ";S;;", [], [])],
            [new _Location("You are in the Wawel Castle", "41.gif", "rgb(255,176,141)", ";;;E", [], []),//4 //smok
            new _Location("You are inside a dragon's cave", "42.gif", "rgb(198,205,193)", ";;W;E", [], []),
            new _Location("A perfect place to set a trap", "43.gif", "rgb(255,176,141)", "N;;W;", [], []),
            new _Location("You are by the water mill", "44.gif", "rgb(255,190,99)", ";;;E", [["a BAG", "1", "BAG", 21]], []),
            new _Location("You are at a main crossroad", "45.gif", "rgb(255,190,99)", "N;S;W;E", [], []),
            new _Location("You are on a town street", "46.gif", "rgb(255,190,99)", "N;;W;E", [], []),
            new _Location("You are in a frontyard of your house", "47.gif", "rgb(255,190,99)", "N;S;W;", [], [])],//main lokalizacja
            [new _Location("pusta lokalizacja"), new _Location("pusta lokalizacja"), new _Location("pusta lokalizacja"),//5
            new _Location("You are by a swift stream", "54.gif", "rgb(108,181,242)", ";;;E", [], []),
            new _Location("You are on a street leading forest", "55.gif", "rgb(255,190,99)", "N;S;W;", [["a KNIFE", "1", "KNIFE", 33]], []),
            new _Location("You are in a woodcutter's backyard", "56.gif", "rgb(255,190,99)", ";S;;", [], []),
            new _Location("You are in a shoemaker's house", "57.gif", "rgb(254,194,97)", "N;;;")],
            [new _Location("pusta lokalizacja"), new _Location("pusta lokalizacja"), new _Location("pusta lokalizacja"),//6
            new _Location("You are in a bleak funeral house", "64.gif", "rgb(254,194,97)", ";;;;E", [["a SPADE", "1", "SPADE", 24]], []),
            new _Location("You are on a path leading to the wood", "65.gif", "rgb(167,245,63)", "N;;W;E", [], []),
            new _Location("You are at the edge of a forest", "66.gif", "rgb(167,245,63)", "N;;W;E", [], []),
            new _Location("You are in a deep forest", "67.gif", "rgb(140,253,99)", ";;W;", [], [])]
        ],
        usable: [
            [10, "45", 11, "You opened a tool shed and took an axe", "H"],
            [11, "56", 12, "You cut sticks for sheeplegs", "H"],
            [12, "32", 13, "You prepared legs for your fake sheep", "L", "OK"],
            [14, "23", 15, "The tavern owner paid you money", "H"],
            [15, "26", 16, "The cooper sold you a new barrel", "H"],
            [16, "32", 17, "You made a nice sheeptrunk", "L", "OK"],
            [18, "25", 19, "The butcher gave you wool", "H"],
            [19, "32", 20, "You prepared skin for your fake sheep", "L", "OK"],
            [21, "46", 22, "You used your tools to make a rag", "H"],
            [22, "32", 23, "You made a fake sheephead", "L", "OK"],
            [24, "00", 25, "You are digging... (timeout) and digging... (timeout) That's enough sulphur for you", "H", "OK"],
            [25, "32", 26, "You prepared a solid poison", "L"],
            [27, "10", 28, "You got a bucket full of tar", "H"],
            [28, "32", 29, "You prepared a liquid poison", "L", "OK"],
            [/*gdy zebrane wszystkie przedmioty(6 * OK),*/6, "32", 37, "Your fake sheep is full of poison and ready to be eaten by the dragon"],
            [37, "32", 30, "The dragon noticed your gift... (timeout) The dragon noticed your gift... (timeout) The dragon ate your sheep and died!", "L", "OK"], //- podmiana grafiki na lokacji (martwy smok)!
            [33, "32" /*+zabity smok*/, 34, "You cut a piece of dragon's skin", "H"],
            [34, "46", 35, "You used your tools to make shoes", "H"],
            [35, "30", 36, "The King is impressed by your shoes! (timeout) The King is impressed by your shoes!", "H"],
            [36, "xx", "koniec"]
        ],
        smok: false,
        player: {
            pos_x: 6,
            pos_y: 3,
            command: function () {
                var cmd = document.getElementById("cmdinput")
                cmd.addEventListener("keydown", function (e) {
                    if (cmd.value) {
                        if (e.keyCode == 13) {
                            if (cmd.value[0] == "N" || cmd.value[0] == "S" || cmd.value[0] == "W" || cmd.value[0] == "E") {
                                game.player.go(cmd.value)
                            }
                            else if (cmd.value[0] == "T") {
                                game.player.take(cmd.value)
                            }
                            else if (cmd.value[0] == "D") {
                                game.player.drop(cmd.value)
                            }
                            else if (cmd.value[0] == "U") {
                                game.player.use(cmd.value)
                            }
                            else if (cmd.value[0] == "V") {
                                game.player.vocabulary(cmd.value)
                            }
                            else if (cmd.value[0] == "G") {
                                game.player.gossips(cmd.value)
                            }
                            else {
                                game.player.error("Try another word or V for vocabulary")
                            }
                            cmd.value = ""
                        }
                        if (e.keyCode == 16) {
                            game.player.caps = !game.player.caps
                        }
                    }
                })
            },
            take: function (t) {
                var danepozycji = game.board[this.pos_y][this.pos_x]
                var x = 0 //potrzebne do sprawdzenia czy gracz wpisał item, który znajduje sie na pozycji

                if (t.slice(0, 2) == "T ") { t = t.replace("T ", "") }
                if (t.slice(0, 5) == "TAKE ") { t = t.replace("TAKE ", "") }

                if (danepozycji.item.length > 0) { //czy jest item
                    if (!game.eq) { //warunek dla ekwipunku max 1 przedmiot niesiony
                        for (var i = 0; i < danepozycji.item.length; i++) {
                            if (t == danepozycji.item[i][2]) { //czy wpisana nazwa to nazwa przedmiotu
                                x++
                                if (danepozycji.item[i][1] == 1) { //czy mozna podniesc item (flaga 1)
                                    game.eq = danepozycji.item[i]
                                    game.player.error("You are taking " + danepozycji.item[i][0])
                                    danepozycji.item.splice(i, 1)
                                    setTimeout(function () {
                                        game.player.itemkiNaPozycji()
                                        game.player.itemkiwEq()
                                    }, 1500);
                                }
                                else { game.player.error("You can't carry it") }
                            }
                        }
                        if (x == 0) { game.player.error("There isn't anything like that here") }
                    }
                    else { game.player.error("You are already carrying something") } //gdy gracz juz cos niesie
                }
                else { game.player.error("There is nothing here") } //gdy na pozycji nie ma itemow
            },
            drop: function (d) {
                var danepozycji = game.board[this.pos_y][this.pos_x]

                if (d.slice(0, 2) == "D ") { d = d.replace("D ", "") }
                if (d.slice(0, 5) == "DROP ") { d = d.replace("DROP ", "") }

                if (game.eq) { //czy masz cos w eq
                    if (danepozycji.item.length < 3) { //czy jest mniej niż 3 itemy
                        if (d == game.eq[2]) { //czy wyrzucamy item ktory mamy w eq
                            game.player.error("You are about to drop " + game.eq[0])
                            danepozycji.item.push(game.eq)
                            game.eq = null
                            setTimeout(function () {
                                game.player.itemkiNaPozycji()
                                game.player.itemkiwEq()
                            }, 1500);
                        }
                        else { game.player.error("You are not carrying it") } //gdy wyrzucasz item ktorego nie masz
                    }
                    else { game.player.error("You can't store any more here") } //gdy na pozycji są już 3 itemy
                }
                else { game.player.error("You are not carrying anything!") } //gdy masz puste eq
            },
            blad1: 0,
            use: function (u) {
                var danepozycji = game.board[this.pos_y][this.pos_x]
                var x = this.pos_x
                var y = this.pos_y
                var xy = '' + y + x

                var nic = 0 //do zliczania czy cokolwiek sie stało

                if (u.slice(0, 2) == "U ") { u = u.replace("U ", "") }
                if (u.slice(0, 4) == "USE ") { u = u.replace("USE ", "") }

                if (game.eq) { //czy masz cos w eq
                    if (u == game.eq[2]) { //czy uzywasz itemu ktory masz w eq
                        for (var i = 0; i < game.usable.length; i++) {
                            nic++
                            if (game.usable[i][1] == xy) { //czy jest dobre xy pozycji na ktorej uzywa sie itemu
                                if (game.usable[i][0] == game.eq[3]) {
                                    if (game.usable[i][5] == "OK") {
                                        game.ok++
                                    }
                                    if (game.usable[i][4] == "H") { //do dloni
                                        game.eq = null
                                        game.eq = game.items[game.usable[i][2]]
                                    }
                                    if (game.usable[i][4] == "L") { //na lokacje
                                        game.eq = null
                                        danepozycji.owca.push(game.items[game.usable[i][2]])
                                    }
                                    var tab = game.usable[i][3].split(" (timeout) ")
                                    // console.log(tab)
                                    var time = 1500
                                    // console.log(time)
                                    if (tab.length == 1) { game.player.error(tab[0]) }
                                    else if (tab.length == 2) { game.player.error(tab[0], tab[1]); time = 3000 }
                                    else if (tab.length == 3) { game.player.error(tab[0], tab[1], tab[2]); time = 4500 }

                                    if (game.ok == 7) {
                                        game.board[3][2].zdjecie = "drake.bmp";
                                        setTimeout(function () {
                                            document.getElementById("obraz").style.background = "url(source/img/" + danepozycji.zdjecie + ")"
                                        }, time);
                                    }

                                    setTimeout(function () {
                                        // console.log("SET")
                                        game.player.itemkiNaPozycji()
                                        game.player.itemkiwEq()
                                        if (game.ok == 6) { //po stworzeniu calej owcy
                                            if (game.player.blad1 == 0) { //po zrobieniu itemka owcy
                                                var com = "Your fake sheep is full of poison and ready to be eaten by the dragon"
                                                game.player.error(com, com)
                                                danepozycji.owca = []
                                                game.eq = null
                                                game.eq = game.items[game.usable[14][2]]
                                            }

                                            game.player.blad1 = 1
                                            setTimeout(function () {
                                                game.player.itemkiNaPozycji()
                                                game.player.itemkiwEq()

                                            }, 3000);
                                        }
                                    }, time);
                                    break
                                }

                            }
                            if (game.usable[i][1] == "xx") {
                                if (game.usable[i][0] == game.eq[3]) {
                                    if (game.usable[i][2] == "koniec") {
                                        document.getElementById("koniec1").style.visibility = "visible"
                                        break
                                    }
                                }
                            }
                        }
                        if (nic == 21) { game.player.error("Nothing happened") }

                    }
                    else { game.player.error("You aren't carrying anything like that") } //gdy uzywasz itemu ktorego nie masz
                }
                else { game.player.error("You are not carrying anything!") } //gdy masz puste eq
            },
            vocabulary: function (v) {
                if (v == "VOCABULARY" || v == "V") { game.player.komunikat("vocabulary") }
            },
            gossips: function (g) {
                if (g == "GOSSIPS" || g == "G") { game.player.komunikat("gossips") }
            },
            go: function (direction) {
                var danepozycji = game.board[this.pos_y][this.pos_x]
                var tab = danepozycji.warunek.split(";") //tablica z kierunkowymi warunkami przejscia z mapy_lokacji_1
                var smok = 0
                if (this.pos_y == 3 && this.pos_x == 2 && game.ok != 7 && direction[0] == "W") {
                    game.player.error("You can't go that way...", "The dragon sleeps in a cave!")
                    smok = 1
                }
                // if (direction[0] == tab[0] || direction[0] == tab[1] || direction[0] == tab[2] || direction[0] == tab[3]) {
                //obsługa kierunków "up", "down" itd.
                console.log(tab)
                var error = 0
                if (direction[0] == tab[0]) if (direction == "NORTH" || direction == "N" && this.pos_y != 0) { this.pos_y--; game.player.error("You are going north..."); error = 1 }
                if (direction[0] == tab[1]) if (direction == "SOUTH" || direction == "S" && this.pos_y != 5) { this.pos_y++; game.player.error("You are going south..."); error = 1 }
                if (direction[0] == tab[2]) if (direction == "WEST" || direction == "W" && this.pos_x != 0 && smok == 0) { this.pos_x--; game.player.error("You are going west..."); error = 1 }
                if (direction[0] == tab[3]) if (direction == "EAST" || direction == "E" && this.pos_x != 6) { this.pos_x++; game.player.error("You are going east..."); error = 1 }

                if (error == 0 && smok == 0) {
                    game.player.error("You can't go that way...")
                }
                var that = this
                setTimeout(function () {
                    that.highlight();
                }, 1500);
                // }
            },
            highlight: function () {
                var danepozycji = game.board[this.pos_y][this.pos_x]

                game.player.kompas()
                game.player.itemkiNaPozycji()
                game.player.itemkiwEq()

                document.getElementById("tekst").innerHTML = danepozycji.tekst
                document.getElementById("obraz").style.background = "url(source/img/" + danepozycji.zdjecie + ")"
                document.getElementById("obrazTlo").style.background = danepozycji.kolor
            },
            itemkiNaPozycji: function () {
                var danepozycji = game.board[this.pos_y][this.pos_x]
                var items = danepozycji.item
                var owca = danepozycji.owca
                var przedmioty = ""
                if (items) {
                    for (var i = 0; i < items.length; i++) {
                        przedmioty += items[i][0] + ", "
                    }
                }
                if (owca) {
                    for (var i = 0; i < owca.length; i++) {
                        przedmioty += owca[i][0] + ", "
                    }
                }
                if (przedmioty != "") { document.getElementById("coWidac").innerHTML = "You see " + przedmioty.substring(0, przedmioty.length - 2) }
                else { document.getElementById("coWidac").innerHTML = "You see nothing" }

                game.player.kompas()
            },
            itemkiwEq: function () {
                if (game.eq) { document.getElementById("plecak").innerHTML = "You are carrying " + game.eq[0] }
                else { document.getElementById("plecak").innerHTML = "You are carrying nothing" }
            },
            kompas: function () {
                var danepozycji = game.board[this.pos_y][this.pos_x]
                var tab = danepozycji.warunek.split(";")
                var kkkk = ""
                document.getElementById("south").style.visibility = "hidden"
                document.getElementById("north").style.visibility = "hidden"
                document.getElementById("west").style.visibility = "hidden"
                document.getElementById("east").style.visibility = "hidden"
                for (var i = 0; i < tab.length; i++) { //pokazywanie kierunków na kompasie i w stringu pod obrazkiem
                    if (tab[i] == "N") { kkkk = kkkk + "NORTH" + ", "; document.getElementById("north").style.visibility = "visible" }
                    if (tab[i] == "S") { kkkk = kkkk + "SOUTH" + ", "; document.getElementById("south").style.visibility = "visible" }
                    if (this.pos_y == 3 && this.pos_x == 2 && game.ok == 7) {
                        if (tab[i] == "W") { kkkk = kkkk + "WEST" + ", "; document.getElementById("west").style.visibility = "visible" }
                    }
                    else if (this.pos_y == 3 && this.pos_x == 2 && game.ok != 7) {

                    }
                    else {
                        if (tab[i] == "W") { kkkk = kkkk + "WEST" + ", "; document.getElementById("west").style.visibility = "visible" }
                    }
                    if (tab[i] == "E") { kkkk = kkkk + "EAST" + ", "; document.getElementById("east").style.visibility = "visible" }
                }
                kkkk = kkkk.substring(0, kkkk.length - 2)
                document.getElementById("kierunek").innerHTML = "You can go " + kkkk
            },
            komunikat: function (id) {
                document.getElementById(id).style.visibility = "visible"
                document.getElementById("cmdinput").addEventListener("keydown", function tak() {
                    document.getElementById(id).style.visibility = "hidden"
                    document.getElementById("cmdinput").removeEventListener("keydown", tak)
                    setTimeout(function () {
                        document.getElementById("cmdinput").value = "";
                    }, 1);
                });
            },
            error: function (err, err2, err3) { //komunikaty po wykonaniu czynności
                console.log("0")
                var divCMD = document.getElementById("cmd")
                var backup = divCMD.innerHTML
                divCMD.innerHTML = err
                setTimeout(function () {
                    console.log("1")
                    if (!err2) {
                        divCMD.innerHTML = backup
                        cmd.value = "";
                        document.getElementById("cmdinput").focus()
                        game.player.command();
                    }
                    else {
                        divCMD.innerHTML = err2
                        setTimeout(function () {
                            console.log("2")
                            if (!err3) {
                                divCMD.innerHTML = backup
                                cmd.value = "";
                                document.getElementById("cmdinput").focus()
                                game.player.command();
                            }
                            else {
                                console.log("3")
                                divCMD.innerHTML = err3
                                setTimeout(function () {
                                    divCMD.innerHTML = backup
                                    cmd.value = "";
                                    document.getElementById("cmdinput").focus()
                                    game.player.command();
                                }, 1500);
                            }
                        }, 1500);
                    }
                }, 1500);
            },
            upperCaseF: function (a) { //przekształcenie tekstu na capslock w inpucie
                if (!game.player.caps) {
                    // if (a.value)
                    setTimeout(function () {
                        var backup = a.value
                        var lastLetter = a.value[a.value.length - 1].toUpperCase();
                        a.value = backup.substring(0, a.value.length - 1) + "" + lastLetter
                    }, 1);
                }
                else {
                    // if (a.value)
                    setTimeout(function () {
                        var backup = a.value
                        var lastLetter = a.value[a.value.length - 1].toLowerCase();
                        a.value = backup.substring(0, a.value.length - 1) + "" + lastLetter
                    }, 1);
                }
            },
            caps: false,
            focus: function () {
                with (document.getElementById('cmdinput')) {
                    onblur = function (e) {
                        var elm = e.target;
                        setTimeout(function () { elm.focus() });
                    }
                    onkeydown = function (e) {
                        var key = e.which || e.keyCode;
                        if (key == 9) e.preventDefault();
                    }
                }
            }
        }
    }
    start = {
        init: function () {

            var audio = document.getElementById("mp3")
            audio.currentTime = 0;
            audio.play();
            audio.volume = 1;
            audio.muted = undefined

            var time1 = 600
            var time2 = 180
            var time3 = 180
            // var time1 = 6000
            // var time2 = 22000
            // var time3 = 14000
            document.getElementById("czolowka1").style.visibility = "visible"
            setTimeout(function () {
                document.getElementById("czolowka2").style.visibility = "visible"
                setTimeout(function () {
                    document.getElementById("czolowka3").style.visibility = "visible"
                    setTimeout(function () {
                        document.getElementById("czolowka1").style.visibility = "hidden"
                        document.getElementById("czolowka2").style.visibility = "hidden"
                        document.getElementById("czolowka3").style.visibility = "hidden"
                        audio.pause();
                        audio.currentTime = 0;
                        game.player.focus();
                        game.player.highlight();
                        game.player.command();
                    }, time3);
                }, time2);
            }, time1);
        }
    }
    start.init();
})